import child_process from "child_process";
import fsExtra from "fs-extra";
import path from "path";
import process from "process";
import { repoRoot, OUT_DIR_NAME } from "./constants.js";
import { generateBlogIndex } from "./generateBlogIndex.js";

console.log("Building site");

generateBlogIndex();

const outDir = path.join(repoRoot, OUT_DIR_NAME);
fsExtra.mkdirsSync(outDir);

const pugProc = child_process.exec(`npx pug src --out ${OUT_DIR_NAME}`, { cwd: repoRoot });
pugProc.stdout.pipe(process.stdout);
pugProc.stderr.pipe(process.stderr);

const srcRes = path.join(repoRoot, "src/res");
const dstRes = path.join(outDir, "res");
fsExtra.removeSync(dstRes);
fsExtra.copySync(srcRes, dstRes, { recursive: true });
