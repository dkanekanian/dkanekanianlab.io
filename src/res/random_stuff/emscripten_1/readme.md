This code will output hello world at the start of the program and output a message everything you click
the button.


The command to run for this to work is:

```powershell
em++ .\helloworld.cpp -s WASM=1 -o index.html --shell-file  ..\my_template.html -s NO_EXIT_RUNTIME=1 -s "EXTRA_EXPORTED_RUNTIME_METHODS=['ccall']"
```

It will produce an HTML file as output but it actually needs some modification to add a button
to show you can call the C++ code from the Javascript.

Add this code in the body, before the text output.

```html
<a href="https://gitlab.com/dkanekanian/dkanekanian.gitlab.io/-/tree/master/src/res/random_stuff/emscripten_1">Source Code</a>
<br>
<button onclick="Module.ccall('myFunction', null, null, null);">Click Me</button>
<br>
```

To find out why there are 3 nulls, read the instructional guide here:
https://developer.mozilla.org/en-US/docs/WebAssembly/C_to_wasm
on the step 7 of the Calling a custom function defined in C. 

To run the produced index.html it requires a http server to host it, not just opening
the file in the browser.

