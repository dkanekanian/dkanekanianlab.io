#include <iostream>
#include <emscripten/emscripten.h>

int main()
{
  std::cout << "Hello, world\n";
}

#ifdef __cplusplus
extern "C" {
#endif

  void EMSCRIPTEN_KEEPALIVE myFunction() {
    std::cout << "You pressed my button!\n";
  }

#ifdef __cplusplus
}
#endif

